(function(){
    angular
        .module('ng-html2canvas', [])
        .factory('html2canvas', html2canvas);

    function html2canvas($window){
        return $window.html2canvas;
    }
})();
